<?php


/**
 *  A form to import formatted text created with export.
 */
function uc_ca_import_export_import_form($form_state) {
  $form = array();
  $form['predicate'] = array(
    '#type' => 'textarea',
    '#title' => t('Import a predicate'),
    '#description' => t('Paste an export of a predicate in this box to create a new predicate'),
    '#rows' => 20,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import'),
  );

  return $form;
}

/**
 * Validate handler for import form.
 * @note: we cannot validate based on pid bcoz when we import in a
 * new site or the same site there are chances that the same pid exists.
 *
 * @TODO: Look for a method to validate against existing predicates.
 *
 */
function uc_ca_import_export_import_form_validate($form, &$form_state) {
  eval('$predicate = '.$form_state['values']['predicate'].';');
  $existing = ca_load_predicate($predicate['#pid']);
  if(empty($existing)) {
    form_set_error('predicate', t('The predicate data is not valid import text.'));
  }
}

/**
 *  Submit handler for import form.
 */
function uc_ca_import_export_import_form_submit($form, &$form_state) {
  eval('$predicate = '.$form_state['values']['predicate'].';');
  $predicate['#pid'] = variable_get('ca_predicates_pid', 1);
  variable_set('ca_predicates_pid', $predicate['#pid'] + 1);
  ca_save_predicate($predicate);
  drupal_set_message("The predicate has been saved.");
}

/**
 *  A form to export predicate definitions.
 */
function uc_ca_import_export_export_form(&$form_state) {
  $form_values = isset($form_state['values']) ? $form_state['values'] : array();
  $step = isset($form_state['storage']['step']) ? $form_state['storage']['step'] + 1 : 1;

  $predicate_id = isset($form_values['predicate_id']) ? $form_values['predicate_id'] : '';
  if($predicate_id) {
    $predicate_key = explode('-', $predicate_id);
    $predicate = ca_load_predicate($predicate_key[1]);
  }

  $form['#step'] = $step;
  $form['#prefix'] = t('This form will process a conditional action predicate and export the conditions and actions settings. The export created by this process can be copied and pasted as an import into the current or any other database.');

  switch ($step) {
    case 1: // Select a content type.
      $predicates = uc_ca_import_export_predicates();

      $form['predicate_id'] = array(
        '#title' => t('Predicates'),
        '#type' => 'radios',
        '#options' => $predicates,
        '#description' => t('Select the predicate to export.'),
      );
      break;

    case 2: // Select groups and fields.
      drupal_set_title(t('Export %predicate', array('%predicate' =>$predicate['#title'])));
      $form['predicate'] = array(
        '#type' => 'textarea',
        '#title' => t('Export of %predicate', array('%predicate' =>$predicate['#title'])),
        '#value' => var_export($predicate, true),
        '#rows' => 20,
      );
      break;
  }

  if ($step < 2) {
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Export'),
    );
  }
  $form['step'] = array(
    '#type' => 'value',
    '#value' => $step,
  );
  return $form;
}

/**
 *  Submit handler for export form.
 */
function uc_ca_import_export_export_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  $form_state['storage']['step'] = $form_state['values']['step'];
}